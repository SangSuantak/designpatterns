# Factory Method: Definition
Define an interface for creating an object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses.

![UML](factory_method.gif)

# When and where you would use it
Class constructors exist so that clients can create instances of a class. However, there are situations, where the client does not, or should not, know which one of several candidate classes to instantiate. The Factory Method allows the client to use an interface for creating an object while still retaining control over which class to instantiate.

The key objective of the Factory Method is extensibility. Factory Methods are frequently used in applications that manage, maintain, or manipulate collections of objects that are different but at the same time have many characteristics in common.

A document management system, for example, is more extensible by referencing the documents as a collection of IDocuments. These documents may be Text files, Word documents, Visio diagrams, or legal papers. They are different but each one has an author, a title, a type, a size, a location, a page count, etc. When a new document type is introduced it simply implements the IDocument interface and it will work like the rest of the documents. To support this new document type the Factory Method may or may not have to be adjusted (depending on how it was implemented, i.e. with or without parameters).

Factory Methods are frequently used in ‘manager’ type components, such as, document managers, account managers, permission managers, custom control managers, etc.

In your own projects you probably have methods that return new objects. However, they may or may not be Factory methods. How to you know when a method is a Factory Method? The rules are:
* the method creates a new object
* the method returns an abstract class or interface
* the abstract class or interface is implemented by several classes

# Advantages
* **Reuse**. If I want to instantiate in many places, I don't have to repeat my condition, so when I come to add a new class, I don't run the risk of missing one.
* **Unit-Testability**. I can write 3 tests for the factory, to make sure it returns the correct types on the correct conditions, then my calling class only needs to be tested to see if it calls the factory and then the required methods on the returned class. It needs to know nothing about the implementation of the factory itself or the concrete classes.
* **Extensibility**. When someone decides we need to add a new class to a factory, none of the calling code, neither unit tests or implementation, ever needs to be told. We simply create a new class and extend our factory method. This is the very definition of [Open-Closed Principle](http://www.oodesign.com/open-close-principle.html).