namespace DesignPatterns.Patterns.FactoryMethod
{
    //Define the product

    //Abstract product
    interface ICar
    { }

    //Concrete product
    class Maruti : ICar
    { }

    //Concrete product
    class Hyundai : ICar
    { }

    //Abstract creator
    interface ICarCreator
    {
        //Factory method
        ICar Create();
    }

    //Concrete creator
    class MarutiCreator : ICarCreator
    {
        //Factory method
        public ICar Create()
        {
            return new Maruti();
        }
    }

    //Concrete creator
    class HyundaiCreator : ICarCreator
    {
        //Factory method
        public ICar Create()
        {
            return new Hyundai();
        }
    }

    //Client
    class MainApp
    {
        public static void Main()
        {
            ICarCreator[] carCreators = new ICarCreator[2];
            carCreators[0] = new MarutiCreator();
            carCreators[1] = new HyundaiCreator();

            foreach(var carCreator in carCreators)
            {
                var car = carCreator.Create();
            }

        }
    }
}