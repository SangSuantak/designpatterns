using System;

namespace DesignPatterns.Patterns.FactoryMethod.PizzaFactory
{
    /// <summary>
    /// 1. This is the factory where we create pizzas.
    /// 2. It should be the only part of our application that refers to concrete classes.
    /// </summary>
    public class ChicagoPizzaFactory : PizzaFactory
    {
        protected override Pizza CreatePizza(string type)
        {
            Pizza pizza = null;

            if (type.Equals("cheese"))
            {
                pizza = new ChicagoCheesePizza();
            }
            else if (type.Equals("pepperoni"))
            {
                pizza = new ChicagoPepperoniPizza();
            }
            else if (type.Equals("clam"))
            {
                pizza = new ChicagoClamPizza();
            }
            else if (type.Equals("veg"))
            {
                pizza = new ChicagoVegPizza();
            }

            return pizza;
        }
    }
}