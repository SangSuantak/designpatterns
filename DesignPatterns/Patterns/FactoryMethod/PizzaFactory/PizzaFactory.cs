namespace DesignPatterns.Patterns.FactoryMethod.PizzaFactory
{
    /// <summary>
    /// This is the abstract pizza factory
    /// </summary>
    public abstract class PizzaFactory
    {        
        public Pizza OrderPizza(string type)
        {
            Pizza pizza;

            pizza = CreatePizza(type);

            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();

            return pizza;
        }

        /// <summary>
        /// This is the factory method
        /// Since it has a input parameter, it is a parameterized factory method.
        /// Often, a factory just produces one object and is not parameterized.
        /// Both are valid forms of the pattern.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected virtual Pizza CreatePizza(string type)
        {
            //A default implementation provided. However, if there are any sub-classes,
            //they are free to implement it their own way.
            return new NYCheesePizza();
        }

    }
}