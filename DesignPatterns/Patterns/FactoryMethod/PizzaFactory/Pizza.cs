using System;
using System.Collections.Generic;

namespace DesignPatterns.Patterns.FactoryMethod.PizzaFactory
{
    /// <summary>
    /// This is the product of the factory
    /// </summary>
    public abstract class Pizza
    {
        protected string Name;
        protected string Dough;
        protected string Sauce;
        protected List<string> Toppings = new List<string>();

        public virtual void Prepare()
        {
            Console.WriteLine("Preparing " + Name);
            Console.WriteLine("Tossing dough...");
            Console.WriteLine("Adding sauce...");
            Console.WriteLine("Adding toppings:");
            foreach(var topping in Toppings)
            {
                Console.WriteLine(" " + topping);
            }
        }

        public virtual void Bake()
        {
            Console.WriteLine("Bake for 25 minutes at 350");
        }

        public virtual void Cut()
        {
            Console.WriteLine("Cut the pizza into diagonal slices");
        }

        public void Box()
        {
            Console.WriteLine("Place pizza in official PizzaStore box");
        }

        public string GetName()
        {
            return Name;
        }
    }

    /// <summary>
    /// Below are a few concrete products
    /// </summary>

    public class NYCheesePizza : Pizza
    {
        public NYCheesePizza()
        {
            Name = "NY style cheeze pizza";
            Dough = "Thin crust";
            Sauce = "Marinara sauce";

            Toppings.Add("Bongsa slice");
        }
    }

    public class NYPepperoniPizza : Pizza { }

    public class NYClamPizza : Pizza { }

    public class NYVegPizza : Pizza { }

    public class ChicagoCheesePizza : Pizza
    {
        public ChicagoCheesePizza()
        {
            Name = "Chicago style cheeze pizza";
            Dough = "Thick crust";
            Sauce = "Plum tomato sauce";

            Toppings.Add("Aksa slice");
        }

        public override void Cut()
        {
            Console.WriteLine("Cut the pizza into square slices");
        }
    }

    public class ChicagoPepperoniPizza : Pizza { }

    public class ChicagoClamPizza : Pizza { }

    public class ChicagoVegPizza : Pizza { }

    public class CaliforniaCheesePizza : Pizza
    {
        public CaliforniaCheesePizza()
        {
            Name = "California style cheeze pizza";
            Dough = "Thick & thin crust";
            Sauce = "Ginseng sauce";

            Toppings.Add("Aksa slice");
        }

        public override void Cut()
        {
            Console.WriteLine("Cut the pizza into triangular slices");
        }
    }

    public class CaliforniaPepperoniPizza : Pizza { }

    public class CaliforniaClamPizza : Pizza { }

    public class CaliforniaVegPizza : Pizza { }
}