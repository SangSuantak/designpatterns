# Factory Method: Definition
Ensure a class has only one instance and provide a global point of access to it.

![UML](singleton.gif)

# Notes
* Static constructors in C# are specified to execute only when an instance of the class is created or a static member is referenced, and to execute only once per AppDomain

# Where to use
* Logging
* Caching
* Load balancer