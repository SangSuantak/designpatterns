﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Singleton
{
    /// <summary>
    /// For .NET 4.0 and above with thread-safety
    /// </summary>
    public sealed class Singleton
    {
        private static readonly Lazy<Singleton> _lazy =
            new Lazy<Singleton>(() => new Singleton());

        private Singleton() { }

        public static Singleton GetInstance()
        {            
            return _lazy.Value;
        }
    }

    /// <summary>
    /// Singleton class in its raw form without thread-safety
    /// </summary>
    public sealed class SingletonRaw
    {
        private static SingletonRaw _instance;

        /// <summary>
        /// To disable instance creation by other class
        /// </summary>
        private SingletonRaw() { }

        public static SingletonRaw GetInstance()
        {
            if(_instance == null)
            {
                _instance = new SingletonRaw();
            }

            return _instance;
        }
    }
}
