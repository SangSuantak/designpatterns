﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Decorator
{
    class Client
    {
        public static void Main()
        {
            Beverage beverage = new Espresso();
            Console.WriteLine(beverage.GetDescription()
                + " $" + beverage.Cost());

            //Create Dark Roast Coffee with double Mocha and Whip
            Beverage beverage2 = new DarkRoast();
            beverage2 = new Mocha(beverage2);
            beverage2 = new Mocha(beverage2);
            beverage2 = new Whip(beverage2);
            Console.WriteLine(beverage2.GetDescription()
                + " $" + beverage2.Cost());

            //Create HouseBlend with Soy, Mocha and Whip
            Beverage beverage3 = new HouseBlend();
            beverage3 = new Soy(beverage3);
            beverage3 = new Mocha(beverage3);
            beverage3 = new Whip(beverage3);
            Console.WriteLine(beverage3.GetDescription()
                + " $" + beverage3.Cost());
        }
    }
}
