﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Decorator
{
    /// <summary>
    /// This is a Concrete Decorator
    /// </summary>
    class Mocha : CondimentDecorator
    {
        private Beverage _beverage;

        public Mocha(Beverage beverage)
        {
            _beverage = beverage;
        }

        public override double Cost()
        {
            return 0.20 + _beverage.Cost();
        }

        public override string GetDescription()
        {
            return _beverage.GetDescription() + ", Mocha";
        }
    }
}
