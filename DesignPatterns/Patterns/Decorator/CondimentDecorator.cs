﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Decorator
{
    /// <summary>
    /// This is the abstract decorator
    /// </summary>
    public abstract class CondimentDecorator : Beverage
    {
        public new abstract string GetDescription();
    }
}
