﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Decorator
{
    /// <summary>
    /// This is a Concrete Component
    /// </summary>
    class DarkRoast : Beverage
    {
        public DarkRoast()
        {
            description = "Dark Roasted Coffee";
        }

        public override double Cost()
        {
            return 1.99;
        }
    }
}
