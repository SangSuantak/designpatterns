# Decorator: Definition
The decorator pattern attaches additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality.

![UML](decorator.gif)

* The `ConcreteComponent` is the object we're going to dynamically add new behavior to. It extends `Component`.
* Each component can be used on its own, or wrapped by a decorator.
* Each `Decorator` **HAS-A** (wraps) a component, which means the decorator has an instance variable that holds a reference to a component.
* Decorators implement the same interface or abstract class as the component they are going to decorate.
* The `ConcreteDecorator` has an instance variable for the thing it decorates (the Component the Decorator wraps).
* Decorators can add new methods; however, new behavior is typically added by doing computation before or after an existing method in the component.

# Notes
* Decorators have the same supertype as the objects they decorate.
* You can use one or more decorators to wrap an object.
* Given that the decorator has the same supertype as the object it decorates, we can pass around a decorated object in place of the original (wrapped) object.
* The decorator adds its own behavior either before and/or after delegating to the object it decorates to do the rest of the job.
* Objects can be decorated at any time, so we can decorate objects dynamically at runtime with as many decorators as we like.
* Decorators can result in many small objects in our design, and overuse can be complex.
