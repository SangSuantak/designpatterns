﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Decorator
{
    /// <summary>
    /// This is a Concrete Decorator
    /// </summary>
    class Soy : CondimentDecorator
    {
        private Beverage _beverage;

        public Soy(Beverage beverage)
        {
            _beverage = beverage;
        }

        public override string GetDescription()
        {
            return _beverage.GetDescription() + ", Soy";
        }

        public override double Cost()
        {
            return 0.13 + _beverage.Cost();
        }

    }
}
