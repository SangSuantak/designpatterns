﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Decorator
{
    /// <summary>
    /// This is a Concrete Component
    /// </summary>
    class HouseBlend : Beverage
    {
        public HouseBlend()
        {
            description = "House Blend Coffee";
        }

        public override double Cost()
        {
            return 0.89;
        }

    }
}
