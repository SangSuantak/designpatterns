﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Decorator
{
    /// <summary>
    /// This is a Concrete Decorator
    /// </summary>
    class Whip : CondimentDecorator
    {
        private Beverage _beverage;

        public Whip(Beverage beverage)
        {
            _beverage = beverage;
        }

        public override string GetDescription()
        {
            return _beverage.GetDescription() + ", Whip";
        }

        public override double Cost()
        {
            return 0.25 + _beverage.Cost();
        }

    }
}
