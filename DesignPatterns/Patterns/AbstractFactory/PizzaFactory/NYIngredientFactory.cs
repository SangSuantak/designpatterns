﻿namespace DesignPatterns.Patterns.AbstractFactory.PizzaFactory
{
    public class NYIngredientFactory : IIngredientFactory
    {
        public Cheese CreateCheese()
        {
            return new ReggianoCheese();
        }

        public Clam CreateClam()
        {
            return new FreshClam();
        }

        public Dough CreateDough()
        {
            return new ThinCrustDough();
        }

        public Sauce CreateSauce()
        {
            return new MarinaraSauce();
        }
    }
}
