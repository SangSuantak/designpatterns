﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.AbstractFactory.PizzaFactory
{
    class CheesePizza : Pizza
    {
        IIngredientFactory ingredientFactory;
        public CheesePizza(IIngredientFactory ingredientFactory)
        {
            this.ingredientFactory = ingredientFactory;
        }

        public override void Prepare()
        {
            dough = ingredientFactory.CreateDough();
            sauce = ingredientFactory.CreateSauce();
            cheese = ingredientFactory.CreateCheese();
        }

    }
}
