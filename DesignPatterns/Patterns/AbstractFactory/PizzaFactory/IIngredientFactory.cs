﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.AbstractFactory.PizzaFactory
{
    public interface IIngredientFactory
    {
        Dough CreateDough();
        Cheese CreateCheese();
        Sauce CreateSauce();
        Clam CreateClam();
    }
}
