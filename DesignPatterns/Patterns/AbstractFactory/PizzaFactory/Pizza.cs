using System;
using System.Collections.Generic;

namespace DesignPatterns.Patterns.AbstractFactory.PizzaFactory
{
    /// <summary>
    /// This is the product of the factory
    /// </summary>
    public abstract class Pizza
    {
        protected string name;
        protected Dough dough;
        protected Sauce sauce;
        protected Cheese cheese;
        protected Clam clam;

        public abstract void Prepare();

        public virtual void Bake()
        {
            Console.WriteLine("Bake for 25 minutes at 350");
        }

        public virtual void Cut()
        {
            Console.WriteLine("Cut the pizza into diagonal slices");
        }

        public void Box()
        {
            Console.WriteLine("Place pizza in official PizzaStore box");
        }

        public string GetName()
        {
            return name;
        }
    }    
}