namespace DesignPatterns.Patterns.AbstractFactory.PizzaFactory
{
    /// <summary>
    /// 1. This is the factory where we create pizzas.
    /// 2. It should be the only part of our application that refers to concrete classes.
    /// </summary>
    public class NYPizzaFactory : PizzaFactory
    {
        protected override Pizza CreatePizza(string type)
        {
            Pizza pizza = null;
            IIngredientFactory ingredientFactory = new NYIngredientFactory();

            if (type.Equals("cheese"))
            {
                pizza = new CheesePizza(ingredientFactory);
            }            
            else if (type.Equals("clam"))
            {
                pizza = new ClamPizza(ingredientFactory);
            }
            
            return pizza;
        }
    }
}