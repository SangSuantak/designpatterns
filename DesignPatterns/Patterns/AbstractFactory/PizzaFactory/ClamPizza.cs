﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.AbstractFactory.PizzaFactory
{
    class ClamPizza : Pizza
    {
        IIngredientFactory ingredientFactory;
        public ClamPizza(IIngredientFactory ingredientFactory)
        {
            this.ingredientFactory = ingredientFactory;
        }

        public override void Prepare()
        {
            dough = ingredientFactory.CreateDough();
            sauce = ingredientFactory.CreateSauce();
            cheese = ingredientFactory.CreateCheese();
            clam = ingredientFactory.CreateClam();
        }
    }
}
