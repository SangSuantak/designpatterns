﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.AbstractFactory.PizzaFactory
{
    public class PizzaClient
    {
        public static void Main()
        {
            PizzaFactory nyFactory = new NYPizzaFactory();
            PizzaFactory chicagoFactory = new ChicagoPizzaFactory();

            Pizza pizza = nyFactory.OrderPizza("cheese");

            pizza = chicagoFactory.OrderPizza("cheese");
        }
    }
}
