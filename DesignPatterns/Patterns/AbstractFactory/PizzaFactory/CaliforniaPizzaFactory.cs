﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.AbstractFactory.PizzaFactory
{
    class CaliforniaPizzaFactory : PizzaFactory
    {
        protected override Pizza CreatePizza(string type)
        {
            Pizza pizza = null;

            if (type.Equals("cheese"))
            {
                pizza = new CaliforniaCheesePizza();
            }
            else if (type.Equals("pepperoni"))
            {
                pizza = new CaliforniaPepperoniPizza();
            }
            else if (type.Equals("clam"))
            {
                pizza = new CaliforniaClamPizza();
            }
            else if (type.Equals("veg"))
            {
                pizza = new CaliforniaVegPizza();
            }

            return pizza;
        }
    }
}
