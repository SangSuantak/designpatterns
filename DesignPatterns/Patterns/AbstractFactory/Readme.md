# Abstract Factory: Definition
Provide an interface for creating families of related or dependent objects without specifying their concrete classes.

![UML](abstract_factory.gif)

* The `Client` is written against the abstract factory and then composed at runtime with an actual factory.
* `AbstractProductA` and `AbstractProductB` are the product family.
* Each concrete factory can product an entire set of products.
* The `AbstractFactory` defines the interface that all concrete factories must implement, which consists of a set of methods for producing products.
* The concrete factories implement the different product families. To create a product, the client uses one of these factories, so it never has to instantiate a product object.

# Important Points
* Provides abstract type for creating a family of products. Subclasses of this type define how those products are produced.
* To use the factory, instantiate and pass it into some code that is written against the abstract type.

# Difference with Factory Method
* Factory method creates objects through inheritance, whereas abstract factory creates objects through object composition.
* Abstract factory create entire families of products, whereas factory method only creates one product.
