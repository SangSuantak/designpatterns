﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Facade
{
    /// <summary>
    /// Subsystem class
    /// </summary>
    class Credit
    {
        public bool HasGoodCredit(Customer customer)
        {
            Console.WriteLine("Check credit for " + customer.Name);
            return true;
        }
    }
}
