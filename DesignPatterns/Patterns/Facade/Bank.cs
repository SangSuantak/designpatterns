﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Facade
{
    /// <summary>
    /// Subsystem class
    /// </summary>
    class Bank
    {
        public bool HasSufficientBalance(Customer customer)
        {
            Console.WriteLine("Check balance for customer " + customer.Name);
            return true;
        }
    }
}
