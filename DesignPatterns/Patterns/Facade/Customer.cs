﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Facade
{
    /// <summary>
    /// Subsystem class
    /// </summary>
    class Customer
    {
        private string _name;
        
        public Customer(string name)
        {
            _name = name;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }
    }
}
