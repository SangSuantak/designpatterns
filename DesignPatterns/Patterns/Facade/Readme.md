# Factory Method: Definition
Provide a unified interface to a set of interfaces in a subsystem. Façade defines a higher-level interface that makes the subsystem easier to use.

![UML](facade.gif)

* Client uses the facade to work.
* Facade provides unifed/simplified interface that is easier to use.
* Subsystem represents complex environment.

# Notes
* With the facade pattern, we can take a complex subsystem and make it easier to use by implementing a Facade class that provides one, more reasonable interface.
* Facades doesn't encapsulate the subsystem classes; they merely provide a simplified interface to their functionality. The subsystem classes still remain available for direct use by clients that need to use more specific interfaces.
* A facade is free to add its own methods to make use of the subsystem.
* Besides simplifying interface, facade also allows you to decouple your client implementation from any one subsystem. Change in subsystem doesn't necessarily result in change in client.

# Difference from Adapter Pattern
The intent of the Adapter pattern is to alter an interface so that it matches one a client is expecting. The intent of the facade pattern is to provide a simplified interface to a subsystem.