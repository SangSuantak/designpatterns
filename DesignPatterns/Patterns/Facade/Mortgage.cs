﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Facade
{
    /// <summary>
    /// Facade class
    /// </summary>
    class Mortgage
    {
        private Bank _bank = new Bank();
        private Loan _loan = new Loan();
        private Credit _credit = new Credit();

        public bool IsEligible(Customer customer, int amount)
        {
            bool eligible = true;

            if(!_bank.HasSufficientBalance(customer))
            {
                eligible = false;
            }
            else if (!_loan.HasNoBadLoans(customer))
            {
                eligible = false;
            }
            else if (!_credit.HasGoodCredit(customer))
            {
                eligible = false;
            }

            return eligible;
        }
    }
}
