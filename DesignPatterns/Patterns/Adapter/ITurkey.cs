﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Adapter
{
    /// <summary>
    /// The 'Adaptee' interface
    /// </summary>
    interface ITurkey
    {
        void Gobble();
        void Fly();
    }
}
