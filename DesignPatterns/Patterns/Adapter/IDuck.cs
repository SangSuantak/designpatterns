﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Adapter
{
    /// <summary>
    /// The 'Target' interface
    /// </summary>
    interface IDuck
    {
        void Quack();
        void Fly();
    }
}
