﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Adapter
{
    class MallardDuck : IDuck
    {
        public void Fly()
        {
            //Duck fly
            throw new NotImplementedException();
        }

        public void Quack()
        {
            //Duck quack
            throw new NotImplementedException();
        }
    }
}
