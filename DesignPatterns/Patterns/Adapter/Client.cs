﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Adapter
{
    class Client
    {
        static void Main()
        {
            //Duck is created
            IDuck duck = new MallardDuck();
            duck.Fly();
            duck.Quack();

            //Turkey is created
            ITurkey turkey = new WildTurkey();
            //Wrap the Turkey in a TurkeyAdapter to make it look like a Duck
            IDuck turkeyAdapter = new TurkeyAdapter(turkey);                       

            //Turkey gobbles and flies
            turkey.Fly();
            turkey.Gobble();

            //Duck is tested
            TestDuck(duck);

            //Turkey is tested but is sent like a duck
            TestDuck(turkeyAdapter);
        }

        static void TestDuck(IDuck duck)
        {
            duck.Quack();
            duck.Fly();
        }
    }
}
