﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Adapter
{
    /// <summary>
    /// The 'Adapter' class
    /// The Adapter implements the Target interface
    /// </summary>
    class TurkeyAdapter : IDuck
    {
        ITurkey turkey = null;
        
        public TurkeyAdapter(ITurkey turkey)
        {
            this.turkey = turkey;
        }

        public void Fly()
        {
            for(int i = 0; i < 5; i++)
            {
                turkey.Fly();
            }            
        }

        public void Quack()
        {
            turkey.Gobble();            
        }
    }
}
