# Adapter: Definition
Convert the interface of a class into another interface clients expect. Adapter lets classes work together that couldn't otherwise because of incompatible interfaces.

![UML](adapter.gif)

* The client sees only the Target interface.
* The Adapter implements the Target interface.
* Adapter is composed with the Adaptee.
* All requests get delegated to the Adaptee.

# Explanation
Just like an adapter is used to connect two imcompatible electrical outlets, in the same manner, an adapter class is used to make two different classes talk to each other using a common interfact/class.

An adapter takes an interface and adapt it to one that a client is expecting.

# Code Explanation
In the example code, we've got two entities, `Duck` and `Turkey`. In order to make the turkey behave/look like a duck, an adapter `TurkeyAdapter` is used. This class implements the `Duck` behaviours and translates them to behaviours understood by the `Turkey`.

## Here's how the Client uses the Adapter
1. The client makes a request to the adapter by calling a method on it using the target interface.
2. The adapter translates that request into one or more calls on the adaptee using the adaptee interface.
3. The client receives the results of the call and never knows there is an adapter doing the translation.

# Where to use
.NET developers write classes that expose methods that are called by clients. Most of the time they will be able to control the interfaces, but there are situations, for example, when using 3rd party libraries, where they may not be able to do so. The 3rd party library performs the desired services but the interface methods and property names are different from what the client expects. This is a scenario where you would use the Adapter pattern.

The Adapter provides an interface the client expects using the services of a class with a different interface. Adapters are commonly used in programming environments where new components or new applications need to be integrated and work together with existing programming components.

Adapters are also useful in refactoring scenarios. Suppose that you have two classes that perform similar functions but have different interfaces. The client uses both classes, but the code would be far cleaner and simpler to understand if they would share the same interface. You cannot alter the interface, but you can shield the differences by using an Adapter which allows the client to communicate via a common interface. The Adapter handles the mapping between the shared interface and the original interfaces.

