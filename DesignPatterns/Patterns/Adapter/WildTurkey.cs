﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns.Patterns.Adapter
{
    class WildTurkey : ITurkey
    {
        public void Fly()
        {
            Console.WriteLine("Turkey flies short distance!!");
        }

        public void Gobble()
        {
            Console.WriteLine("Gobble Gobble!!");
        }
    }
}
